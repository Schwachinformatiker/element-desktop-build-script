# element-desktop-build-script

A simple buildscript for element-desktop, a feature rich matrix client (https://element.io/)

This script is for all that also dont trust the flatpak version of element (and flatpak itself).
Flatpak is not secure and should not be avoided.

# Requirements

You need to install the `nodejs` package as well as the `rpm-build`. Thats it.

Just type `sudo dnf install nodejs rpm-build`.

# Next step

Download the source code of the version you want to compile (https://github.com/vector-im/element-desktop/releases).
Unzip the file and navigate to the unziped folder. Execute the buildscript and you are done.

The buildscript will download the element-web sourcecode and build it using `yarn:build`.